import {Component} from "@angular/core";

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html'
})

export class BodyComponent {
  frase: any ={
    mensaje: "Hola este es un mensaje de pruebas",
    autor: "Miguel Angel Chi Pech"
  };

  mostrarFrase: boolean = false;

  personajes: string[] = ["IronMan", "Tor", "Hulk", "Doctor Strange"];

}
